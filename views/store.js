import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        isLogined: false,
        id: '',
        name: '',
        token: '',
        destroyedBookmark: 0,
        searchTagQuery: '',
        changedTag: 0
    },
    mutations: {
        logIn (state) {
            state.isLogined = true
            state.id = localStorage.id
            state.name = localStorage.name
            state.token = localStorage.token
        },
        logOut (state) {
            state.isLogined = false
            state.id = ''
            state.name = ''
            state.token = ''
        },
        reflectStatus (state) {
            if (localStorage.length) {
                state.isLogined = true
                state.id = localStorage.id
                state.name = localStorage.name
                state.token = localStorage.token
            } else {
                state.isLogined = false
                state.id = ''
                state.name = ''
                state.token = ''
            }
        },
        destroyBookmark (state, id) {
            state.destroyedBookmark =  id
        },
        searchTag (state, tagName) {
            state.searchTagQuery = tagName
        },
        changedTag (state) {
            state.changedTag++
        }
    },
    actions: {
        logIn ({commit}) {
            commit('logIn')
        },
        logOut ({commit}) {
            commit('logOut')
        },
        reflectStatus ({commit}) {
            commit('reflectStatus')
        },
        destroyBookmark({commit}, id) {
            commit('destroyBookmark', id)
        },
        searchTag({commit}, tagName) {
            commit('searchTag', tagName)
        },
        changedTag({commit}) {
            commit('changedTag')
        }
    }
})
