import Vue from 'vue'
import App from './main.vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import store from './store'
import bookmarkList from './bookmark-list.vue'
import bookmarkAdd from './bookmark-add.vue'
import signupRun from './signup-run.vue'
import signinRun from './signin-run.vue'
import firstPage from './first-page.vue'
Vue.use(VueRouter)

const routes = [
    { path: '/list', component: bookmarkList },
    { path: '/list/:offset/:limit', component: bookmarkList },
    { path: '/add', component: bookmarkAdd },
    { path: '/signup', component: signupRun },
    { path: '/signin', component: signinRun },
    { path: '/', component: firstPage }
]

const router = new VueRouter({
    routes
})

new Vue({
    store,
    router,
    render: h => h(App)
}).$mount('#main')
