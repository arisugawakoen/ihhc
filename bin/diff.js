'use strict'

const beforeTagText = process.argv[2]
const afterTagText = process.argv[3]

const beforeTag = beforeTagText.split(' ')
const afterTag = afterTagText.split(' ')

const deleteTag = beforeTag.filter((b) => {
    return afterTag.indexOf(b) == -1
})

const addTag = afterTag.filter((a) => {
    return beforeTag.indexOf(a) == -1
})

console.log(deleteTag)
console.log(addTag)
