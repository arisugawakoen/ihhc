'use strict'

const models = require('../models')
const bookmarkId = process.argv[2]
const tagText = process.argv[3]

const arrTag = tagText.split(' ')
const mapTag = arrTag.map((t) => {
    models.tagmap.sequelize.query(
	    'DELETE FROM tagmaps USING tags,bookmarks WHERE tags.tag_id = tagmaps.tag_id AND tagmaps.bookmark_id = bookmarks.bookmark_id AND bookmarks.bookmark_id = $1 AND tags.name = $2;',
	    { bind: [bookmarkId, t], type: models.sequelize.QueryTypes.SELECT }
    )
})
