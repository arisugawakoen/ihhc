'use strict'

const models = require('../models')
const userId = process.argv[2]

models.tag.sequelize.query(
    'SELECT tags.* FROM tags JOIN tagmaps ON tags.tag_id = tagmaps.tag_id JOIN bookmarks ON tagmaps.bookmark_id = bookmarks.bookmark_id WHERE bookmarks.user_id = $1;',
    { bind: [userId], type: models.sequelize.QueryTypes.SELECT }
).then((tags) => {
    console.log(tags)
    models.sequelize.close()
})
