'use strict'

const models = require('../models')
const bookmarkId = process.argv[2]
const tagText = process.argv[3]

const arrTag = tagText.split(' ')
const mapTag = arrTag.map((t) => {
    models.tag.findOrCreate({
	    where: { name: t }
    }).spread((tag, created) => {
	    models.tagmap.findOrCreate({
	        where: {
		        tag_id: tag.tag_id,
		        bookmark_id: bookmarkId,
	        }
	    })
    })
})
