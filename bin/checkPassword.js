'use strict'

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const models = require('../models')
const myId = process.argv[2]
const myPassword = process.argv[3]
const secret = "secret"

models.User.findOne({
  where:{ 
    name: myId
  },
  attributes: ['user_id', 'name', 'password']
}).then((user) => {
  if (user) {
    bcrypt.compare(myPassword, user.password, (err, res) => {
      if (err) {
        console.log(err)
        return
      }

      console.log(res)

      if (res) {
        console.log(user)
        const token = jwt.sign(user.toJSON(), secret, {
          expiresIn: '24h'
        })
        console.log(token)
      }

      models.sequelize.close()
    })
  } else {
    console.log(user)
    models.sequelize.close()
  }
}).catch((e) => {
  console.log(e)
})
