'use strict'

const models = require('../models')
const userId = process.argv[2]
const tag = process.argv[3]

models.bookmark.sequelize.query(
    'SELECT bookmarks.* FROM bookmarks JOIN tagmaps ON bookmarks.bookmark_id = tagmaps.tag_id JOIN tags ON tags.tag_id = tagmaps.tag_id WHERE bookmarks.user_id= $1 AND tags.name = $2;',
    { bind: [userId, tag], type: models.sequelize.QueryTypes.SELECT }
).then((b) => {
    console.log(b)
    models.sequelize.close()
})
