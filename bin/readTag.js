'use strict'

const models = require('../models')
const bookmarkId = process.argv[2]
const userId = process.argv[3]
let tagText = []
let bookmarkArr = []

models.tag.sequelize.query(
    'SELECT tags.name FROM tags JOIN tagmaps ON tags.tag_id = tagmaps.tag_id JOIN bookmarks ON tagmaps.bookmark_id = bookmarks.bookmark_id WHERE bookmarks.bookmark_id = $1 AND bookmarks.user_id = $2;',
    { bind: [bookmarkId, userId], type: models.sequelize.QueryTypes.SELECT }
).then((tags) => {
    tagText = tags.map(x => x.name)
}).then(() => {
    models.bookmark.findOne({
        raw: true,
        where: {
            user_id: userId,
            bookmark_id: bookmarkId
        }
    }).then((b) => {
        bookmarkArr = b
        bookmarkArr.tag = tagText
    }).then(() => {
        console.log(bookmarkArr)
    })
})
