'use strict'

const client = require('cheerio-httpcli')
const models = require('../models')
client.set('maxDataSize', 10*1024*1024)

client.fetch(process.argv[2])
.then((result) => {
  console.log(result.$.documentInfo().url)
	
  let scrapingResult = new Object()
  
  scrapingResult.date = result.response.headers.date
  scrapingResult.url = process.argv[2]

  if (result.$('title').text()) {
    scrapingResult.title = result.$('title').text()
  }
  if (result.$('meta[name="description"]').attr('content')) {
    scrapingResult.description = result.$('meta[name="description"]').attr('content').replace(/\r?\n/g,"")
  }
/*
  models.Bookmark.findOrCreate({
    where: { url: scrapingResult.url },
    defaults: {
      title: scrapingResult.title,
      description: scrapingResult.description,
      date: scrapingResult.date
    }
  }).spread((bookmark, created) => {
    console.log(created)
    models.sequelize.close()
*/
}).catch((err) => {
  console.log(err)

})
