'use strict'

const models = require('../models')
const bookmarkId = process.argv[2]
const tagText = process.argv[3]

const arrTag = tagText.split(' ')

    Promise.all(arrTag.map((tagT) => {
        models.tag.findOrCreate({
            where: { name: tagT }
        }).spread((t, created) => {
            if (created) {
                models.tagmap.findOrCreate({
                    where: {
                        tag_id: t.tag_id,
                        bookmark_id: bookmarkId
                    }
                })
            }
            console.log(t)
            return t
        }).then((result) => {
            console.log(result)
            return result
        })
    })).then(
        (result) => console.log(result),
        (e) => console.log(e)
    )
