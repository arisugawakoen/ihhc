'use strict'

const bcrypt = require('bcrypt')
const models = require('../models')
const saltRounds = 10
const myId = process.argv[2]
const myPassword = process.argv[3]

bcrypt.hash(myPassword, saltRounds, (err, hash) => {
  if (err) {
    console.log(err)
    models.sequelize.close()
    return
  }

  models.User.findOrCreate({
    where: { name: myId },
    defaults: {
      password: hash
    }
  }).spread((user, created) => {
    console.log(created)
    models.sequelize.close()
  })
})
