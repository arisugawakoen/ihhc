'use strict';
module.exports = (sequelize, DataTypes) => {
  var tag = sequelize.define('tag', {
    tag_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
	autoIncrement: true,
	unique: true
    },
    name: {
      type: DataTypes.TEXT,
      allowNull: false
    },
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return tag;
};
