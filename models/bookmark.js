'use strict';
module.exports = (sequelize, DataTypes) => {
    var bookmark = sequelize.define('bookmark', {
        bookmark_id: {
	        type: DataTypes.INTEGER,
	        primaryKey: true,
	        autoIncrement: true,
	        unique: true
        },
        title: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        url: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        description: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        date: {
            type: DataTypes.DATE,
            allowNull: true
        },
        tag: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
    }, {
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });
    return bookmark;
};
