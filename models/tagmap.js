'use strict';
module.exports = (sequelize, DataTypes) => {
  var tagmap = sequelize.define('tagmap', {
    tagmap_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
	autoIncrement: true,
	unique: true
    },
    tag_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    bookmark_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return tagmap;
};
