"use strict"

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');

var indexRouter = require('./routes/indexApi');
var addRouter = require('./routes/scrapingApi');
var accountRouter = require('./routes/accountApi');
var validRouter = require('./routes/validTokenApi');
var bookmarkIdRouter = require('./routes/idApi');
var tagRouter = require('./routes/tagApi');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/bookmark_id', bookmarkIdRouter);
app.use('/api/tag', tagRouter);
app.use('/api/valid', validRouter);
app.use('/api/account', accountRouter);
app.use('/api/add', addRouter);
app.use('/api', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  res.status(err.status);
  res.end('404: Not Found. The Requested URL ' + req.path + ' was not found on this server.' );
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.send({
    message: err.message,
    error: {}
  });
});

module.exports = app;
