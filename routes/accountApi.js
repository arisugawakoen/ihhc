'use strict'

const express = require('express')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const models = require('../models')
const router = express.Router()
const secret = require('../config/key')

router.post('/signup', (req, res) => {
    const saltRounds = 10

    bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
        if (err) {
            res.status(400).send(e)
        }

        models.user.findOrCreate({
            where: { name: req.body.name },
            defaults: { password: hash }
        }).spread((user, created) => {
            const userInfo = {}
            userInfo.name = user.name
            userInfo.user_id = user.user_id
            if (created) {
                res.status(201).send(userInfo)
            } else {
                res.status(409).send(userInfo)
            }
        })
    })
})

router.post('/signin', (req,res) => {
    models.user.findOne({
        where: {
            name: req.body.name
        },
        attributes: ['user_id', 'name', 'password']
    }).then((user) => {
        if (user) {
            bcrypt.compare(req.body.password, user.password,
                           (err, response) => {
                if (err) {
                    res.status(406).send(err)
                }

                if (response) {
                    const userInfo = {}
                    userInfo.name = user.name
                    userInfo.id = user.user_id
                    const token = jwt.sign(userInfo, secret.key, {
                        expiresIn: '24h'
                    })
                    userInfo.token = token
                    res.status(200).send(userInfo)
                } else {
                    res.status(406).send()
                }
            })
        } else {
            res.status(406).send()
        }
    }).catch((e) => {
        res.status(400).send(e)
    })
})

module.exports = router
