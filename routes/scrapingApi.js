'use strict'

const express = require('express')
const client = require('cheerio-httpcli')
const jwt = require('jsonwebtoken')
const models = require('../models')
const router = express.Router()
const secret = require('../config/key')
client.set('maxDataSize', 10*1024*1024)
client.set('timeout', 10000)

router.use((req, res, next) => {
    const token = req.headers['x-access-token']

    if (!token) {
        return res.status(401).send({
            success: false,
            message: 'No token provided.'
        })
    } else {
        jwt.verify(token, secret.key, (err, decoded) => {
            if(err) {
                return res.status(401).send(err)
            } else {
                req.decoded = decoded
                next()
            }
        })
    }
})

router.post('/', (req, res) => {
    let scrapingResult = new Object()
    const encodedUrl = encodeURI(req.body.url)

    client.fetch(encodedUrl)
        .then((result) => {
            scrapingResult.date = result.response.headers.date
            scrapingResult.userId = req.decoded.id
            scrapingResult.url = decodeURI(result.$.documentInfo().url)

            if (result.$('title').text()) {
                scrapingResult.title = result.$('title').text()
            } else {
                scrapingResult.title = 'No title'
            }

            if (result.$('meta[name="description"]').attr('content')) {
                scrapingResult.description
                    = result.$('meta[name="description"]').attr('content')
                    .replace(/\r?\n/g,"")
            } else {
                scrapingResult.description = 'No description'
            }

            const token = jwt.sign(scrapingResult, secret.key, {
                expiresIn: '1h'
            })
            
            scrapingResult.token = token

            return scrapingResult
        }).then((scrapingResult) => {
            return JSON.stringify(scrapingResult)
        }).then((jsonResult) => {
            res.json(jsonResult)
        }).catch((e) => {
            res.status(400).send(e)
        })
    }
)

router.post('/confirm', (req,res) => {
    jwt.verify(req.body.token, secret.key, (err, decoded) => {
        if(err) {
            res.status(400).send(err)
        }

        const tagText = req.body.tag.trim().replace(/ +/g, ' ')
        
        models.bookmark.findOrCreate({
            where: { 
                url: decoded.url,
                user_id: decoded.userId
            },
            defaults: {
                title: req.body.title,
                description: req.body.description,
                date: decoded.date,
                tag: tagText
            }
        }).spread((bookmark, created) => {
            if (created) {
                res.status(201).send(bookmark)
            } else {
                res.status(303).send(bookmark)
            }
        })
    })
})

module.exports = router
