'use strict'

const express = require('express')
const jwt = require('jsonwebtoken')
const router = express.Router()
const models = require('../models')
const secret = require('../config/key')

router.use((req, res, next) => {
    const token = req.headers['x-access-token']

    if (!token) {
        return res.status(401).send({
            success: false,
            message: 'No token provided.'
        })
    } else {
        jwt.verify(token, secret.key, (err, decoded) => {
            if(err) {
                return res.status(401).send(err)
            } else {
                req.decoded = decoded
                next()
            }
        })
    }
})

router.get('/:offset(\\d+)/:limit(\\d+)', (req, res) => {
    const offset = parseInt(req.params.offset)
    const limit = parseInt(req.params.limit)

    models.bookmark.findAll({
        offset: offset,
        limit: limit,
        where: {
            user_id: req.decoded.id
        },
        order: models.sequelize.literal('bookmark_id DESC'),
        attributes: ['bookmark_id',
                     'url',
                     'title',
                     'description',
                     'tag',
                     'date']
    }).then((articles) => {
        return JSON.stringify(articles)
    }).then((result) => {
        res.json(result)
    }).catch((e) => {
        res.status(400).json(e)
    })
})

router.get('/search', (req, res) => {
    const searchTag = req.query.tag

    models.bookmark.sequelize.query(
        'SELECT bookmarks.* FROM bookmarks JOIN tagmaps ON tagmaps.bookmark_id = bookmarks.bookmark_id JOIN tags ON tags.tag_id = tagmaps.tag_id WHERE bookmarks.user_id = $1 AND tags.name = $2;',
        { bind: [req.decoded.id, searchTag],
          type: models.sequelize.QueryTypes.SELECT }
    ).then((articles) => {
        return JSON.stringify(articles)
    }).then((result) => {
        res.json(result)
    }).catch((e) => {
        res.sendStatus(400)
    })
})

router.get('/', (req, res) => {
    models.bookmark.findAll({
        where: {
            user_id: req.decoded.id
        },
        order: models.sequelize.literal('bookmark_id DESC'),
        attributes: ['bookmark_id',
                     'url',
                     'title',
                     'description',
                     'tag',
                     'date']
    }).then((articles) => {
        return JSON.stringify(articles)
    }).then((result) => {
        res.json(result)
    }).catch((e) => {
        res.status(400).json(e)
    })
})

module.exports = router
