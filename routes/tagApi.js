'use strict'

const express = require('express')
const jwt = require('jsonwebtoken')
const router = express.Router()
const models = require('../models')
const secret = require('../config/key')

router.use((req, res, next) => {
    const token = req.headers['x-access-token']

    if (!token) {
        return res.status(401).send({
            success: false,
            message: 'No token provided.'
        })
    } else {
        jwt.verify(token, secret.key, (err, decoded) => {
            if(err) {
                return res.status(401).send(err)
            } else {
                req.decoded = decoded
                next()
            }
        })
    }
})

router.get('/:bookmarkId(\\d+)', (req, res) => {
    const bookmarkId = parseInt(req.params.bookmarkId)

    models.tag.sequelize.query(
        'SELECT tags.* FROM tags JOIN tagmaps ON tags.tag_id = tagmaps.tag_id JOIN bookmarks ON tagmaps.bookmark_id = bookmarks.bookmark_id WHERE bookmarks.bookmark_id = $1 AND bookmarks.user_id = $2;',
        { bind: [bookmarkId, req.decoded.id],
          type: models.sequelize.QueryTypes.SELECT }
    ).then((tags) => {
        return JSON.stringify(tags)
    }).then((result) => {
        res.json(result)
    }).catch((e) => {
        res.status(400).json(e)
    })
})

router.get('/', (req, res) => {
    models.tag.sequelize.query(
        'SELECT DISTINCT tags.* FROM tags JOIN tagmaps ON tags.tag_id = tagmaps.tag_id JOIN bookmarks ON tagmaps.bookmark_id = bookmarks.bookmark_id WHERE  bookmarks.user_id = $1;',
        { bind: [req.decoded.id],
          type: models.sequelize.QueryTypes.SELECT }
    ).then((tags) => {
        return JSON.stringify(tags)
    }).then((result) => {
        res.json(result)
    }).catch((e) => {
        res.status(400).json(e)
    })
})

router.delete('/:bookmarkId(\\d+)', (req, res) => {
    const bookmarkId = parseInt(req.params.bookmarkId)
    const arrTag = req.body.tags

    arrTag.map((t) => {
        if (t.length) {
            models.tagmap.sequelize.query(
                'DELETE FROM tagmaps USING tags,bookmarks WHERE tags.tag_id = tagmaps.tag_id AND tagmaps.bookmark_id = bookmarks.bookmark_id AND bookmarks.bookmark_id = $1 AND tags.name = $2 AND bookmarks.user_id = $3;',
                { bind: [bookmarkId, t, req.decoded.id],
                  type: models.sequelize.QueryTypes.SELECT }
            ).catch((e) => {
                console.log(e)
            })
        }
    })
    res.sendStatus(200)    
})

router.delete('/tag_name', (req, res) => {
    models.tagmap.sequelize.query(
        'DELETE FROM tagmaps USING tags,bookmarks WHERE tags.tag_id = tagmaps.tag_id AND tagmaps.bookmark_id = bookmarks.bookmark_id AND tags.name = $1 AND bookmarks.user_id = $2;',
        { bind: [req.body.tag, req.decoded.id],
          type: models.sequelize.QueryTypes.SELECT }
    ).then(() => {
        models.bookmark.sequelize.query(
            "UPDATE bookmarks SET tag = TRIM(REGEXP_REPLACE((REPLACE(tag, $1, '')), ' {2,}', ' ', 'g')) WHERE user_id = $2;",
            { bind: [req.body.tag, req.decoded.id],
              type: models.sequelize.QueryTypes.SELECT }
        ).catch((e) => {
            res.sendStatus(409).json(e)
        })
    }).then(() => {
        res.sendStatus(204)
    }).catch((e) => {
        res.sendStatus(409).json(e)
    })
})  

router.put('/:bookmarkId(\\d+)', (req, res) => {
    const bookmarkId = parseInt(req.params.bookmarkId)
    const arrTag = req.body.tags

    if (arrTag.length) {
        arrTag.map((tagText) => {
            if (tagText.length) {
                models.tag.findOrCreate({
                    where: { name: tagText }
                }).spread((t, created) => {
                    models.tagmap.findOrCreate({
                        where: {
                            tag_id: t.tag_id,
                            bookmark_id: bookmarkId
                        }
                    }).catch((e) => {
                        console.log(e)
                    })
                }).catch((e) => {
                    console.log(e)                
                })
            }            
        })
    }

    res.sendStatus(200)
})

module.exports = router
