'use strict'

const express = require('express')
const jwt = require('jsonwebtoken')
const router = express.Router()
const models = require('../models')
const secret = require('../config/key')

router.use((req, res, next) => {
    const token = req.headers['x-access-token']

    if (!token) {
        return res.status(401).send({
            success: false,
            message: 'No token provided.'
        })
    } else {
        jwt.verify(token, secret.key, (err, decoded) => {
            if(err) {
                return res.status(401).send(err)
            } else {
                req.decoded = decoded
                next()
            }
        })
    }
})

router.get('/:bookmarkId(\\d+)', (req, res) => {
    const bookmarkId = parseInt(req.params.bookmarkId)

    models.bookmark.findOne({
        where: {
            user_id: req.decoded.id,
            bookmark_id: bookmarkId
        },
        attributes: ['bookmark_id',
                     'user_id',
                     'url',
	                 'title',
                     'description',
                     'tag',
                     'date']
    }).then((articles) => {
        return JSON.stringify(articles)
    }).then((result) => {
        res.json(result)
    }).catch((e) => {
        res.status(400).json(e)
    })
})

router.delete('/:bookmarkId(\\d+)', (req, res) => {
    const bookmarkId = parseInt(req.params.bookmarkId)

    models.bookmark.destroy({
        where: {
            user_id: req.decoded.id,
            bookmark_id: bookmarkId
        }
    }).then(() => {
        models.tagmap.sequelize.query(
            'DELETE FROM tagmaps USING tags,bookmarks WHERE tags.tag_id = tagmaps.tag_id AND tagmaps.bookmark_id = bookmarks.bookmark_id AND bookmarks.bookmark_id = $1 AND bookmarks.user_id = $2;',
            { bind: [bookmarkId, req.decoded.id],
              type: models.sequelize.QueryTypes.SELECT }
        ).catch((e) => {
            console.log(e)
        })
        res.sendStatus(204)
    }).catch((e) => {
        res.status(400).json(e)
    })
})

router.put('/:bookmarkId(\\d+)', (req, res) => {
    const bookmarkId = parseInt(req.params.bookmarkId)

    models.bookmark.update({
        title: req.body.title,
        description: req.body.description,
        tag: req.body.tag
    } , {
        where: {
            user_id: req.decoded.id,
            bookmark_id: bookmarkId
        }
    }).then(() => {
        res.sendStatus(204)
    }).catch((e) => {
        res.status(409).json(e)
    })
})

module.exports = router
