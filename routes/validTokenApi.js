'use strict'

const express = require('express')
const jwt = require('jsonwebtoken')
const models = require('../models')
const router = express.Router()
const secret = require('../config/key')

router.get('/',(req, res) => {
    const token = req.headers['x-access-token']

    if (!token) {
        return res.status(401).send({
            success: false,
            message: 'No token provided.'
        })
    } else {
        jwt.verify(token, secret.key, (err, decoded) => {
            if(err) {
                return res.status(401).send({
                    success: false,
                    message: 'Invalid token.'
	            })
            } else {
                return res.status(200).send({
                    success: true,
                    message: 'Valid token.'
                })
            }
        })
    }
})

module.exports = router
